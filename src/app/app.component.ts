import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  	title = 'Blog';

  
  	postDate(){
  		this.date = new Date();
  		return this.date;
  	}

 	posts=[
	  {
	  title: 'title 1',
	  content: 'content 1',
	  loveIts: 0,
	  created_at: this.postDate()
	  },
	  {
	  title: 'title 2',
	  content: 'content 2',
	  loveIts: 0,
	  created_at: this.postDate()
	  },
	  {
	  title: 'title 3',
	  content: 'content 3',
	  loveIts: 0,
	  created_at: this.postDate()
	  },
 	];

 	onInit(){
  		//console.log('go');
  	}

}
