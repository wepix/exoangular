import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

	@Input() postTitle: string;
	@Input() postContent: string;
	@Input() postLoveIts: string;
  @Input() postDate: Date;
  @Input() postLike: number;


	constructor() { }

	ngOnInit() {
	}


 	onLike(){
    this.postLoveIts++;
  	//this.postLoveIts = 'liked';
  	console.log(this.postLoveIts);
  }
  onDislike(){
    this.postLoveIts--;
    //this.postLoveIts = 'disliked';
    console.log(this.postLoveIts);
  }

  getLike(){
  	return this.postLoveIts;
  }

}
